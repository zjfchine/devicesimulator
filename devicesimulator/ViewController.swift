//
//  ViewController.swift
//  devicesimulator
//
//  Created by zjf on 01/08/2015.
//  Copyright (c) 2015 zjf. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {
    
    var vmacquisition: VMAcquisitionController?
    var urlDataSource: LoginUrlDS = LoginUrlDS()
    
    @IBOutlet weak var camPreview: NSView!
    @IBOutlet weak var timeLabel: NSTextField!
    
    @IBOutlet weak var loginUrlCombo: NSComboBox!
    @IBOutlet weak var txtDeviceId: NSTextField!
    

    @IBAction func loginAction(sender: AnyObject) {
        var url: String = loginUrlCombo.stringValue.trim()
        var deviceId:String = txtDeviceId.stringValue.trim()
        if deviceId == "" {
            alert("device id is empty")
            return;
        }
        
        vmacquisition?.deviceLogin(url, deviceId:deviceId)
    }
    
    @IBAction func startAction(sender: AnyObject) {
        vmacquisition?.startAcquisition()
    }
    
    @IBAction func stopAction(sender: AnyObject) {
        vmacquisition?.stopAcquisition()
    }
        
    @IBAction func alertAction(sender: AnyObject) {
        vmacquisition?.triggerAlert()
    }
    
    @IBAction func stopAlertAction(sender: AnyObject) {
        vmacquisition?.stopAlert()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        vmacquisition = VMAcquisitionController(cameraView:camPreview, timeLabel:timeLabel)
        
        loginUrlCombo.usesDataSource = true
        loginUrlCombo.dataSource = urlDataSource
        loginUrlCombo.selectItemAtIndex(0)
        
        
    }

    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

