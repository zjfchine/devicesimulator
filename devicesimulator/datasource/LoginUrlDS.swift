//
//  LoginUrlDS.swift
//  devicesimulator
//
//  Created by zjf on 01/12/2015.
//  Copyright (c) 2015 zjf. All rights reserved.
//

import Cocoa


class LoginUrlDS : NSObject, NSComboBoxDataSource, NSComboBoxDelegate {
    var urls : [String] = [ "http://192.168.0.90:8090/CMSWeb/services/iotapp/device/login.do",
        "http://192.168.0.111:8090/CMSWeb/services/iotapp/device/login.do",
        "http://127.0.0.1:8090/CMSWeb/services/iotapp/device/login.do",
        "http://115.29.223.231:8090/CMSWeb/services/iotapp/device/login.do",
        "http://123.85.190.220:8001/CMSWeb/services/iotapp/device/login.do" ]
    
    func comboBox(aComboBox: NSComboBox, objectValueForItemAtIndex index: Int) -> AnyObject {
        return urls[index];
    }
    
    func numberOfItemsInComboBox(aComboBox: NSComboBox) -> Int {
        return urls.count
    }
    
    
    func comboBox(aComboBox: NSComboBox, completedString uncompletedString: String) -> String?{
        for index in 0..<urls.count {
            if urls[index].has(uncompletedString) {
                return urls[index]
            }
        }
        return ""
        
    }
    
    func comboBox(aComboBox: NSComboBox, indexOfItemWithStringValue aString: String) -> Int {
        for index in 0..<urls.count {
            if urls[index].has(aString) {
                return index
            }
        }
        return NSNotFound
    }
    
    func addUrl(url:String!){
        var exist:Bool = false
        for str in urls {
            if str == url {
                exist = true
                break
            }
        }
        if !exist {
            urls.append(url)
        }
    }
}

